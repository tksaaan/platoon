<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ApplyUsersTable105537 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            //
            $table->text('area_1')->nullable()->change();
            $table->text('area_2')->nullable()->change();
            $table->text('area_3')->nullable()->change();
            $table->text('profile')->nullable()->change();
            $table->text('thumbnail')->nullable()->change();
       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //
            $table->text('area_1')->nullable(false)->change();
            $table->text('area_2')->nullable(false)->change();
            $table->text('area_3')->nullable(false)->change();
            $table->text('profile')->nullable(false)->change();
            $table->text('thumbnail')->nullable(false)->change();
        });
    }
}
