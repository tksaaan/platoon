<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ApplyUsersTable104103 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            //
            $table->text('area_1');
            $table->text('area_2');
            $table->text('area_3');
            $table->text('profile');
            $table->text('thumbnail');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //
            $table->dropColumn('area_1');
            $table->dropColumn('area_2');
            $table->dropColumn('area_3');
            $table->dropColumn('profile');
            $table->dropColumn('thumbnail');
        });
    }
}
