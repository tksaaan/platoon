<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FieldAreaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('field', function (Blueprint $table) {
            //
            $table->increments("id");
            $table->text("field_name");
            $table->text("prefecture");
            $table->text("address");
            $table->text("near_station");
            $table->text("url");
            $table->tinyInteger('outdoor_flg');
            $table->tinyInteger('urban_flg');
            $table->tinyInteger('forest_flg');
            $table->tinyInteger('indoor_flg');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('field');
    }
}
