<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Auth::routes();
/*
    Auth::routes();　で以下のルートが自動で追加される。
    // ログイン・ログアウト
    $this->get('login', 'Auth\LoginController@showLoginForm')->name('login');
    $this->post('login', 'Auth\LoginController@login');
    $this->post('logout', 'Auth\LoginController@logout')->name('logout');

    // 会員登録
    $this->get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
    $this->post('register', 'Auth\RegisterController@register');

    // PASSリセット
    $this->get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
    $this->post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
    $this->get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
    $this->post('password/reset', 'Auth\ResetPasswordController@reset');
*/
Route::get('/hq', 'HomeController@index')->name('hq');

//仮登録　確認画面
Route::post('/register/pre_check', 'Auth\RegisterController@pre_check')->name('register.pre_check');

//本登録画面
Route::get('/register/verify/{token}', 'Auth\RegisterController@showForm');

//本登録　確認gamen
Route::post('/register/main_check', 'Auth\RegisterController@mainCheck')->name('register.main.check');

Route::post('/register/main_register', 'Auth\RegisterController@mainRegister')->name('register.main.registered');


Route::group(['middleware' => ['auth']], function () {
    // この中はログインされている場合のみルーティングされる    
    
    //マイページTOP
    Route::get('/hq/setting', 'MypageController@index')->name('hq.mypage');
    //プロフィール編集
    Route::get('/hq/profile', 'User\UserController@getProfile')->name('hq.profile');
    Route::post('/hq/profile', 'User\UserController@postProfile')->name('hq.profile');
    //プロフィール編集
    Route::get('/hq/unsubscribe', 'User\UserController@getDeleteProfile')->name('hq.unsubscribe');
    Route::post('/hq/unsubscribe', 'User\UserController@postDeleteProfile')->name('hq.unsubscribe');


    //フィールド検索結果
    Route::get('/hq/search-result', 'SearchFieldController@postSearchResult')->name('hq.search-result');
    //各フィールドページ
    Route::get('/hq/field', 'SearchFieldDetailController@postFieldDetail')->name('hq.field-detail');

});

