<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Log;

use Auth;
use Validator;

class UserController extends Controller
{
    //
    public $user;

    public function __construct() 
    {
        $this->middleware('auth');
    }

    //プロフィール変更（GET）
    public function getProfile() 
    {
        $this->user = Auth::User();
        return view('hq.profile')->with(['user' => $this->user]);
    }

    //プロフィール変更（POST）
    public function postProfile(Request $request) 
    {
        $this->user = Auth::User();
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:12',
        ]);

        //error check
        if ($validator->fails())
        {
            return back()->withInput()->withErrors($validator);
        }

        //update
        $this->user->update([
            'name' => $request->input('name'),
        ]);
        
        return redirect('/hq/setting');

    }

    //プロフィール変更（GET）
    public function getDeleteProfile() 
    {
        $this->user = Auth::User();
        return view('hq.unsubscribe');
    }

    //プロフィール変更（POST）
    public function postDeleteProfile(Request $request) 
    {
        $this->user = Auth::User('id')->delete();
        return redirect('/');
    }


}
