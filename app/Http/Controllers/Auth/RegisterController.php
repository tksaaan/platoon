<?php

namespace App\Http\Controllers\Auth;

use App\Mail\EmailVerification;
use App\User;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Log;

use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Auth\Events\Registered;
use Illuminate\Validation\ValidationException;

class RegisterController extends Controller
{
  /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */
  use RegistersUsers;
  /**
   * Where to redirect users after registration.
   *
   * @var string
   */
  protected $redirectTo = '/hq';

  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
    $this->middleware('guest');
  }
  /**
   * Get a validator for an incoming registration request.
   *
   * @param  array  $data
   * @return \Illuminate\Contracts\Validation\Validator
   */
  protected function validator(array $data)
  {
    return Validator::make($data, [
      //            'name' => 'required|string|max:255',
      'email' => 'required|string|email|max:255|unique:users',
      'password' => 'required|string|min:6|confirmed',
    ]);
  }
  public function pre_check(Request $request)
  {
    $this->validator($request->all())->validate();
    //flash data
    $request->flashOnly('email');
    $bridge_request = $request->all();
    // password マスキング
    $bridge_request['password_mask'] = '******';
    return view('auth.register_check')->with($bridge_request);
  }
  /**
   * Create a new user instance after a valid registration.
   *
   * @param  array  $data
   * @return \App\User
   */
  protected function create(array $data)
  {
    $user = User::create([
      'email' => $data['email'],
      'password' => Hash::make($data['password']),
      'email_verify_token' => base64_encode($data['email']),
    ]);

    $email = new EmailVerification($user);
    Mail::to($user->email)->send($email);
    return $user;
  }

  public function register(Request $request)
  {
    event(new Registered($user = $this->create($request->all())));
    return view('auth.registered');
  }
  /**
   * 本会員登録 入力画面
   * @param $email_token
   * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
   */
  public function showForm($email_token)
  {
    // 使用可能なトークンか
    if (!User::where('email_verify_token', $email_token)->exists()) {
      return view('auth.main.register')->with('message', '無効なトークンです。');
    } else {

      //トークンでDB参照
      $user = User::where('email_verify_token', $email_token)->first();

      // 本登録済みユーザーか
      if ($user->status == config('const.USER_STATUS.REGISTER')) //REGISTER=1
      {
        logger("status" . $user->status);
        return view('auth.main.register')->with('message', 'すでに本登録されています。ログインして利用してください。');
      }

      // ユーザーステータス更新
      $user->status = config('const.USER_STATUS.MAIL_AUTHED');

      //saveが成功したら次の画面
      if ($user->save()) {
        return view('auth.main.register', compact('email_token'));
      } else {
        return view('auth.main.register')->with('message', 'メール認証に失敗しました。再度、メールからリンクをクリックしてください。');
      }
    }
  }

  public function mainCheck(Request $request)
  {
    $time = Carbon::now();
    $request->validate([
      'name' => 'required|string',
      'sex' => 'required|integer',
      'birth_year' => 'required|numeric',
      'birth_month' => 'required|numeric',
      'birth_day' => 'required|numeric',
      'thumbnail' => 'file|image|mimes:jpeg,png,jpg,gif|max:2048',
    ]);

    //データ保持用
    $email_token = $request->email_token;

    // ユーザーインスタンスの作成
    $user = new User();

    // 名前
    $user->name = $request->name;

    // 性別
    $user->sex = $request->sex;

    // 生年月日
    $user->birth_year = $request->birth_year;
    $user->birth_month = $request->birth_month;
    $user->birth_day = $request->birth_day;

    // サムネイル
    $user->thumbnail = $request->thumbnail->storeAs('public/post_images', $time->timestamp . '.jpg');

    // プロフィール
    $user->profile = $request->profile;

    //Log::debug($request);
    return view('auth.main.register_check', compact('user', 'email_token'));
  }

  public function mainRegister(Request $request)
  {

    $user = User::where('email_verify_token', $request->email_token)->first();

    $user->status = config('const.USER_STATUS.REGISTER');
    $user->name = $request->name;
    $user->sex = $request->sex;
    $user->birth_year = $request->birth_year;
    $user->birth_month = $request->birth_month;
    $user->birth_day = $request->birth_day;
    $user->profile = $request->profile;

    //ユーザー画像がアップロードされていなければデフォルト画像（男女で分ける）
    if ($user->thumbnail === null || $user->thumbnail === "") {

      // S3のサムネイル保存ディレクトリのパス
      $img_dir_path = config('const.AWS_S3_DIR') . '/thumb';

      // 男
      if ($user->sex === "1") {

        $user->thumbnail =  $img_dir_path . '/default-icon_male.jpg';
      }
      // 女
      else {

        $user->thumbnail =  $img_dir_path . '/default-icon_female.jpg';
      }
    }
    // サムネイルが指定されていたらS3へアップロード
    else {

      $file_name = str_replace('public', 'storage', $request->thumbnail);
      $imgPath = Storage::disk('s3')->putFile('/thumb', new \Illuminate\Http\File($file_name), 'public');
      $user->thumbnail = Storage::disk('s3')->url($imgPath);
    }

    // DB登録
    $user->save();

    // 登録完了画面へ遷移
    return view('auth.main.registered');
  }
}
