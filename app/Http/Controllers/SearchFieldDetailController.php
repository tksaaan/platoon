<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

use Validator;
use App\Field;

class SearchFieldDetailController extends Controller
{

    //
    public function __construct() {

    }

    public function postFieldDetail(Request $request) {

        $query = Field::query();                
        $query->where(function($q) use($request) {

            $q->orWhere("id", "=", $request->field_detail);

        });

        $results = $query->get();

        //$validator = Validator::make($request->all(), [
        //    'prefecture' => 'required_without_all',
        //]);

        //error check
        //if ($validator->fails())
        //{
        //    return back()->withInput()->withErrors($validator);
        //}

        //update
        //$this->user->update([
        //  ]);
        
        return view('hq.field-detail')->with('results', $results[0]);

    }    
}
