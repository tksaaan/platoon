<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;


class MypageController extends Controller
{
    //
    public function index() {
        $user = Auth::user();
        return view('hq.mypage', compact('user'));
    }
}
