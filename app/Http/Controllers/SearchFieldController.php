<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

use Validator;
use App\Field;

class SearchFieldController extends Controller
{
    //
    public function __construct() {

    }

    public function postSearchResult(Request $request) {

        $query = Field::query();
        
        if(is_array($request->input('prefecture'))){

            $query->where(function($q) use($request) {
            
                foreach ($request->input("prefecture") as $param) {

                    $q->orWhere("prefecture", "like", "%". $param. "%");

                }

            });

        }

        $results = $query->get();

        if( count($results) === 0) {
            
            $results = 0;
            
        }
        
        //$validator = Validator::make($request->all(), [
        //    'prefecture' => 'required_without_all',
        //]);

        //error check
        //if ($validator->fails())
        //{
        //    return back()->withInput()->withErrors($validator);
        //}

        //update
        //$this->user->update([
        //  ]);
        
        return view('hq.search-result')->with('results', $results);

    }

}
