<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class EmailVerification extends Mailable
{
    use Queueable, SerializesModels;

    protected $user;
    /**
     * Create a new message instance.
     *
     * @return void
     */

    //User情報を渡す
    public function __construct($user)
    {
        //これでbuild()の処理で
        //$this->user->email_verify_tokenと参照することができます。
        //（ユーザーごとのtokenを判別できます）
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->subject('【PLATOON!!】仮登録のお知らせ')
            ->view('auth.email.pre_register')
            ->with(['token' => $this->user->email_verify_token]);
    }
}
