<!DOCTYPE html>
<html lang="jp">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>PLATOON!!｜サバゲー仲間をカンタンに探せる</title>
    <link rel="stylesheet" href="css/reset.css">
    <link rel="stylesheet" href="css/common.css">
    <link rel="stylesheet" href="css/top.css">
</head>
<body>

  <main id="topPage">
      <section class="main-visual">
          {{-- <a class="logo" href="/">
            <img src="img/logo.svg" alt="PLATOON!!">
          </a> --}}
        <div class="title">
          <h1>背中を預ける仲間が欲しいか？</h1>
          <p>ネットで話題のサバイバルゲーム専用SNS</p>          
          {{-- <p>全国のサバゲーマーを探せるSNS</p> --}}
  
          <div class="apply-area">
              <p>ベータ版稼働中！</p>
              <div class="btn-area">
                <a class="btn register" href="/register">会員登録</a>
                <a class="btn login" href="/login">ログイン</a>
              </div>
              </div>
        </div>

          </section>

      <!--
      <section class="content">
        <div class="txt">
            <h2>関東圏のフィールドを完全網羅。</h2>
            <p>各フィールドの特徴が一目でわかる！</p>
        </div>
        <div class="img"></div>
      </section>
    -->
      <!--
      <section class="content">
          <div class="txt">
        <h2>初心者歓迎！ビギナー向けコラム掲載中</h2>
        <p>「サバゲーが初めてで、何から始めればいいの？」<br>心配ありません。超初心者が初めてサバゲーデビューするまでの方法を徹底解説！</p>
        </div>
        <div class="img"></div>
          </section>
        -->
    </main>

</body>
</html>