<!-- MYPAGE TOP -->
@extends('layouts.mypage')

@section('title')
  PLATOON!!
@stop

@section('content')
<h1>{{$results->field_name}}</h1>
<ul class="search-flg-label-list">
    @if($results->outdoor_flg === 1)
        <li class="outdoor">室外フィールド</li>
    @endif

    @if($results->urban_flg === 1)
        <li class="urban">市街フィールド</li>
    @endif

    @if($results->forest_flg === 1)
        <li class="forest">森林フィールド</li>
    @endif

    @if($results->indoor_flg === 1)
        <li class="indoor">インドアフィールド</li>
    @endif
</ul>

<div>
    <b>住所</b>：{{$results->prefecture}}{{$results->address}}
</div>
<div>
    <b>最寄駅</b>：{{$results->near_station}}駅
</div>

<h2>こんな人がこのフィールドでメンバーを探しています！</h2>
aaa

@endsection