<!-- MYPAGE TOP -->
@extends('layouts.mypage')

@section('title')
    退会処理｜PLATOON!!
@stop


@section('content')
<h1>退会処理</h1>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/hq/unsubscribe') }}">
                        @csrf
                        
                        <p>
                            PLATOON!!からあなたの登録情報を削除します。<br>
                            ※一度退会すると、登録した情報を元に戻すことはできません。
                        </p>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    退会する
                                </button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection