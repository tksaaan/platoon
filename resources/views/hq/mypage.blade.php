<!-- MYPAGE TOP -->
@extends('layouts.mypage')

@section('title')
  マイページ設定｜PLATOON!!
@stop

@section('content')
<div>
    <h1>マイページ設定</h1>
    <div>
        <h2>
            <a href="/hq/profile">
                プロフィール変更
            </a>
        </h2>
        <p>
            ユーザー名を変更します。
        </p>
    </div>
    {{-- 
    <div>
        <h2>
            <a href="">
                作成チーム登録情報変更
            </a>
        </h2>
        <p>
            作成したチームの紹介文などを変更します。
        </p>
    </div>
    --}}
     <div>
        <h2>
            <a href="/hq/unsubscribe">
                アカウント削除
            </a>
        </h2>
        <p>
            退会処理を行います。
        </p>
    </div>
</div>
@endsection