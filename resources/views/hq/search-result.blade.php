<!-- MYPAGE TOP -->
@extends('layouts.mypage')

@section('title')
  検索結果｜PLATOON!!
@stop

@section('content')
<h1>検索結果</h1>
<div class="container">

                    @if($results !== 0)
                        
                        @foreach($results as $result)
                            <div class="field-box">
                                <h2>
                                    <form id="searchFieldForm" class="form-horizontal" role="form" method="GET" action="/hq/field">
                                        @csrf
                                        <button type="submit" value="{{$result->id}}" name="field_detail">{{$result->field_name}}</button>
                                    </form>                                    
                                </h2>
                                <ul class="search-flg-label-list">
                                    @if($result->outdoor_flg === 1)
                                        <li class="outdoor">室外フィールド</li>
                                    @endif

                                    @if($result->urban_flg === 1)
                                        <li class="urban">市街フィールド</li>
                                    @endif

                                    @if($result->forest_flg === 1)
                                        <li class="forest">森林フィールド</li>
                                    @endif

                                    @if($result->indoor_flg === 1)
                                        <li class="indoor">インドアフィールド</li>
                                    @endif
                                </ul>

                                <p>
                                </p>
                                
                                <div>
                                    <b>住所</b>：{{$result->prefecture}}{{$result->address}}
                                </div>
                                <div>
                                    <b>最寄駅</b>：{{$result->near_station}}駅
                                </div>
                            </div>
                        @endforeach
                    
                    @else
                    
                            <p>該当する結果がありませんでした。</p>
                    
                    @endif

                    <a class="button01" href="/hq/">TOPへ戻る</a>
</div>
@endsection