<!-- MYPAGE TOP -->
@extends('layouts.mypage')

@section('title')
  PLATOON!!
@stop

@section('content')
<h1>ヒットコールは大声でね、{{ Auth::user()->name }}！</h1>

  <h2>フィールドを探す</h2>
  <form id="prefectureForm" class="form-horizontal" role="form" method="GET" action="{{ url('/hq/search-result') }}">
    @csrf
    <fieldset class="prefecture">
      <legend>都道府県を選択</legend>
      {{--
      <label><input type="checkbox" name="prefecture[]" value="北海道">北海道</label>
      <label><input type="checkbox" name="prefecture[]" value="青森">青森</label>
      <label><input type="checkbox" name="prefecture[]" value="岩手">岩手</label>
      <label><input type="checkbox" name="prefecture[]" value="宮城">宮城</label>
      <label><input type="checkbox" name="prefecture[]" value="秋田">秋田</label>
      <label><input type="checkbox" name="prefecture[]" value="山形">山形</label>
      <label><input type="checkbox" name="prefecture[]" value="福島">福島</label>
      --}}
      <label><input type="checkbox" name="prefecture[]" value="茨城">茨城</label>
      <label><input type="checkbox" name="prefecture[]" value="栃木">栃木</label>
      <label><input type="checkbox" name="prefecture[]" value="群馬">群馬</label>
      <label><input type="checkbox" name="prefecture[]" value="埼玉">埼玉</label>
      <label><input type="checkbox" name="prefecture[]" value="千葉">千葉</label>
      <label><input type="checkbox" name="prefecture[]" value="東京">東京</label>
      <label><input type="checkbox" name="prefecture[]" value="神奈川">神奈川</label>
      <label><input type="checkbox" name="prefecture[]" value="山梨">山梨</label>
      <label><input type="checkbox" name="prefecture[]" value="長野">長野</label>
      {{-- 
      <label><input type="checkbox" name="prefecture[]" value="新潟">新潟</label>
      <label><input type="checkbox" name="prefecture[]" value="富山">富山</label>
      <label><input type="checkbox" name="prefecture[]" value="石川">石川</label>
      <label><input type="checkbox" name="prefecture[]" value="福井">福井</label>
      <label><input type="checkbox" name="prefecture[]" value="岐阜">岐阜</label>
      <label><input type="checkbox" name="prefecture[]" value="静岡">静岡</label>
      <label><input type="checkbox" name="prefecture[]" value="愛知">愛知</label>
      <label><input type="checkbox" name="prefecture[]" value="三重">三重</label>
      <label><input type="checkbox" name="prefecture[]" value="滋賀">滋賀</label>
      <label><input type="checkbox" name="prefecture[]" value="京都">京都</label>
      <label><input type="checkbox" name="prefecture[]" value="大阪">大阪</label>
      <label><input type="checkbox" name="prefecture[]" value="兵庫">兵庫</label>
      <label><input type="checkbox" name="prefecture[]" value="奈良">奈良</label>
      <label><input type="checkbox" name="prefecture[]" value="和歌山">和歌山</label>
      <label><input type="checkbox" name="prefecture[]" value="鳥取">鳥取</label>
      <label><input type="checkbox" name="prefecture[]" value="島根">島根</label>
      <label><input type="checkbox" name="prefecture[]" value="岡山">岡山</label>
      <label><input type="checkbox" name="prefecture[]" value="広島">広島</label>
      <label><input type="checkbox" name="prefecture[]" value="山口">山口</label>
      <label><input type="checkbox" name="prefecture[]" value="徳島">徳島</label>
      <label><input type="checkbox" name="prefecture[]" value="香川">香川</label>
      <label><input type="checkbox" name="prefecture[]" value="愛媛">愛媛</label>
      <label><input type="checkbox" name="prefecture[]" value="高知">高知</label>
      <label><input type="checkbox" name="prefecture[]" value="福岡">福岡</label>
      <label><input type="checkbox" name="prefecture[]" value="佐賀">佐賀</label>
      <label><input type="checkbox" name="prefecture[]" value="長崎">長崎</label>
      <label><input type="checkbox" name="prefecture[]" value="熊本">熊本</label>
      <label><input type="checkbox" name="prefecture[]" value="大分">大分</label>
      <label><input type="checkbox" name="prefecture[]" value="宮崎">宮崎</label>
      <label><input type="checkbox" name="prefecture[]" value="鹿児島">鹿児島</label>
      <label><input type="checkbox" name="prefecture[]" value="沖縄">沖縄</label>
      --}}

      <button type="submit">検索</button>

    </fieldset>
    
    
    {{-- <legend>絞り込み検索</legend>
    <fieldset class="option">
      <label><input type="checkbox" name="option[]" value="ageLimit">20歳未満OK</label>
      <label><input type="checkbox" name="option[]" value="recommendFemale">女性が多い</label>
      <label><input type="checkbox" name="option[]" value="beginner">初心者歓迎</label>
    </fieldset> --}}
  </form>

  <script>
    $(function() {
      
      const input = $(".prefecture").find('input[type="checkbox"]');
      input.on("click", function() {
        
        if( $(this).prop("checked") === true ) {
          
          $(this).parent("label").addClass("checked");
          
        } else {
          
          $(this).parent("label").removeClass("checked");
                    
        }
        
      });
      
    });
    </script>
  
@endsection