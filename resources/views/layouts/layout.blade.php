<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>@yield('title')</title>
  @yield('styles')
</head>
<body>
<header>
  <nav class="my-navbar">
    <a class="my-navbar-brand" href="/">PLATOON!!</a>
  </nav>
</header>
<main>
  @yield('content')
</main>
<footer>
  @yield('footer')
</footer>
@yield('scripts')
</body>
<!--テンプレート。＠yieldの部分をほかのページで＠sectionで呼び出す-->
</html>