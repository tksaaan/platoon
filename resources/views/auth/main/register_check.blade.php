@extends('layouts.app')

@section('title')
  本登録確認｜PLATOON!!
@stop

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">本会員登録確認</div>

                <div class="card-body">
                    <form enctype="multipart/form-data" method="POST" action="{{ route('register.main.registered') }}" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="email_token" value="{{ $email_token }}">

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">名前</label>
                            <div class="col-md-6">
                                <span class="">{{$user->name}}</span>
                                <input type="hidden" name="name" value="{{$user->name}}">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="sex" class="col-md-4 col-form-label text-md-right">性別</label>
                            <div class="col-md-6">
                                <span class="">
                                    <?php
                                        if($user->sex === "1"){
                                          echo("男性");
                                        } else {
                                          echo("女性");
                                        }
                                    ?>
                                </span>
                                <input type="hidden" name="sex" value="{{$user->sex}}">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="birth" class="col-md-4 col-form-label text-md-right">生年月日</label>
                            <div class="col-md-6">
                                <span class="">{{$user->birth_year}}年</span>
                                <input type="hidden" name="birth_year" value="{{$user->birth_year}}">
                                <span class="">{{$user->birth_month}}月</span>
                                <input type="hidden" name="birth_month" value="{{$user->birth_month}}">
                                <span class="">{{$user->birth_day}}日</span>
                                <input type="hidden" name="birth_day" value="{{$user->birth_day}}">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="profile" class="col-md-4 col-form-label text-md-right">プロフィール画像</label>
                            <div class="col-md-6">
                                <span class="">
                                    <img style="width:100px;" src="/{{str_replace('public', 'storage', $user->thumbnail)}}"></span>
                                <input type="hidden" name="thumbnail" value="{{$user->thumbnail}}">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="profile" class="col-md-4 col-form-label text-md-right">ひとこと</label>
                            <div class="col-md-6">
                                <span class="">{{$user->profile}}</span>
                                <input type="hidden" name="profile" value="{{$user->profile}}">
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    本登録
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection