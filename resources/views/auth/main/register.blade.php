@extends('layouts.app')

@section('title')
  本登録フォーム｜PLATOON!!
@stop


@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">本会員登録</div>

                    @isset($message)
                        <div class="card-body">
                            {{$message}}
                        </div>
                    @endisset

                    @empty($message)
                        <div class="card-body">
                            <form method="POST" action="{{ route('register.main.check') }}" enctype="multipart/form-data">
                                @csrf

                                <input type="hidden" name="email_token" value="{{ $email_token }}">

                                <div class="form-group row">
                                    <label for="name" class="col-md-4 col-form-label text-md-right">名前</label>
                                    <div class="col-md-6">
                                        <input
                                            id="name" type="text"
                                            class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}"
                                            name="name" value="{{ old('name') }}" required>

                                        @if ($errors->has('name'))
                                            <span class="invalid-feedback">
                                            <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="sex" class="col-md-4 col-form-label text-md-right">性別</label>

                                    <div class="col-md-6">
                                        <label>
                                            <input
                                                    id="sex0" type="radio"
                                                    class="form-control{{ $errors->has('sex') ? ' is-invalid' : '' }}"
                                                    name="sex" value="1{{ old('sex') }}" required>男性
                                        </label>
                                        <label>
                                            <input
                                                id="sex1" type="radio"
                                                class="form-control{{ $errors->has('sex') ? ' is-invalid' : '' }}"
                                                name="sex" value="2{{ old('sex') }}" required>女性
                                        </label>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="name_pronunciation"
                                           class="col-md-4 col-form-label text-md-right">生年月日</label>
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <select id="birth_year" class="form-control" name="birth_year" required>
                                                    <option value="">----</option>
                                                    @for ($i = 1980; $i <= 2005; $i++)
                                                        <option value="{{ $i }}"
                                                                @if(old('birth_year') == $i) selected @endif>{{ $i }}</option>
                                                    @endfor
                                                </select>
                                                @if ($errors->has('birth_year'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('birth_year') }}</strong>
                                                    </span>
                                                @endif
                                            </div>年

                                            <div class="col-md-3">
                                                <select id="birth_month" class="form-control" name="birth_month" required>
                                                    <option value="">--</option>
                                                    @for ($i = 1; $i <= 12; $i++)
                                                        <option value="{{ $i }}"
                                                            @if(old('birth_month') == $i) selected @endif>{{ $i }}</option>
                                                    @endfor
                                                </select>
                                                @if ($errors->has('birth_month'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('birth_month') }}</strong>
                                                    </span>
                                                @endif
                                            </div>月

                                            <div class="col-md-3">
                                                <select id="birth_day" class="form-control" name="birth_day" required>
                                                    <option value="">--</option>
                                                    @for ($i = 1; $i <= 31; $i++)
                                                        <option value="{{ $i }}"
                                                            @if(old('birth_day') == $i) selected @endif>{{ $i }}</option>
                                                    @endfor
                                                </select>

                                                @if ($errors->has('birth_day'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('birth_day') }}</strong>
                                                    </span>
                                                @endif
                                            </div>日
                                        </div>

                                        <div class="row col-md-6 col-md-offset-4">
                                            @if ($errors->has('birth'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('birth') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="thumbnail" class="col-md-4 col-form-label text-md-right">プロフィール画像</label>
                                    <div class="col-md-6">
                                        <input
                                            id="thumbnail" type="file"
                                            class="form-control{{ $errors->has('thumbnail') ? ' is-invalid' : '' }}"
                                            name="thumbnail" value="{{ old('thumbnail') }}" required>

                                        @if ($errors->has('thumbnail'))
                                            <span class="invalid-feedback">
                                            <strong>{{ $errors->first('thumbnail') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="profile" class="col-md-4 col-form-label text-md-right">ひとこと</label>
                                    <div class="col-md-6">
                                        <textarea
                                            id="profile" type="text"
                                            class="form-control{{ $errors->has('profile') ? ' is-invalid' : '' }}"
                                            name="profile" value="{{ old('profile') }}">
                                        </textarea>

                                        @if ($errors->has('profile'))
                                            <span class="invalid-feedback">
                                            <strong>{{ $errors->first('profile') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row mb-0">
                                    <div class="col-md-6 offset-md-4">
                                        <button type="submit" class="btn btn-primary">
                                            確認画面へ
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    @endempty
                </div>
            </div>
        </div>
    </div>
@endsection