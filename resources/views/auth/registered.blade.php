@extends('layouts.app')

@section('title')
  メールをご確認ください｜PLATOON!!
@stop

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">確認メールを送信しました</div>

                <div class="card-body">
                    <p>ご登録いただき、誠にありがとうございます。</p>
                    <p>
                        ご本人様確認のため、ご登録いただいたメールアドレスに、<br>
                        本登録のご案内のメールが届きます。
                    </p>
                    <p>
                        そちらに記載されているURLにアクセスし、<br>
                        アカウントの本登録を完了させてください。
                    </p>
                    <a href="{{url('/')}}" class="sg-btn">TOPページ</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection